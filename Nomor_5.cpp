/*

Quick Sort
Oleh Zulfahmi Trimahardika
NIM: 201011400434

*/

#include <bits/stdc++.h> 
using namespace std;

void swap(int* a, int* b) {
	int t = *a;
	*a = *b;
	*b = t;
}

int partition (int arr[], int awal, int akhir) {
	int pivot = arr[akhir];
	int i = (awal - 1);
	
	for (int j = awal; j <= akhir - 1; j++) {
		if (arr[j] <= pivot) {
			i++;
			swap(&arr[i], &arr[j]);
		}
	}
	swap(&arr[i + 1], &arr[akhir]);
	return (i + 1);
}

void quickSort(int arr[], int awal, int akhir) {
	if (awal < akhir) {
		int pi = partition(arr, awal, akhir);
		quickSort(arr, awal, pi - 1);
		quickSort(arr, pi + 1, akhir);
	}
}

void printArray(int arr[], int size) {
	int i;
	for (i = 0; i < size; i++)
	cout << arr[i] << " ";
	cout << endl;
}

int main() {
	int arr[] = {12, 7, 9, 10, 13, 15, 16, 1};
	int n = sizeof(arr) / sizeof(arr[0]);
	quickSort(arr, 0, n - 1);
	cout << "Array terurut: \n";
	printArray(arr, n);
	return 0;
}
