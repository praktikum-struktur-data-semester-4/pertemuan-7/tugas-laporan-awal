/*

Program menginput data dan menampilkan bilangan genap saja

Oleh Zulfahmi Trimahardika
NIM: 201011400434

*/
#include <iostream>
using namespace std;

void bubbleSort(int arr[], int n) {
	int i, j;
	for (i = 0; i < n - 1; i++)
	
	for (j = 0; j < n - i - 1; j++)
	if (arr[j] > arr[j + 1])
	swap(arr[j], arr[j + 1]);
}

int main() {
	int values[10], jmldata;
	cout<<"Jumlah data: ";cin>>jmldata;
	cout<<"Memasukan Data >>"<<endl;
	for(int i=0;i<jmldata;i++){
		cout<<"Data ke "<<i+1<<": ";
		cin>>values[i];
	}
	
	cout<<endl<<"Data yang anda masukan adalah: "<<endl;
	for(int i=0;i<jmldata;i++){
		cout<<values[i]<<" ";
	}
	
	cout<<endl<<endl<<"Data bilangan genap dari yang diinput: "<<endl;
	
	for(int i = 0; i < jmldata; i++) {
	    if(values[i] % 2 == 0) {
		    bubbleSort(values, jmldata);
			cout<<values[i]<<" ";
	    }else if (values[i] % 2 != 0){
	    	continue;
		}
	}

  return 0;
}
