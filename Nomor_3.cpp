/*

Bubble Sort
Oleh Zulfahmi Trimahardika
NIM: 201011400434

*/

#include <bits/stdc++.h>
using namespace std;

void bubbleSort(int arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	
	for (j = 0; j < n - i - 1; j++)
	if (arr[j] > arr[j + 1])
	swap(arr[j], arr[j + 1]);
}

void cetakArray(int arr[], int size)
{
	int i;
	for (i = 0; i < size; i++)
	cout << arr[i] << " ";
	cout << endl;
}

int main()
{
	int arr[] = {12, 7, 9, 10, 13, 15, 16, 1};
	int N = sizeof(arr) / sizeof(arr[0]);
	cout << "Array sebelum diurut: \n";
	cetakArray(arr, N);
	cout << endl;
	bubbleSort(arr, N);
	cout << "Array setelah diurut: \n";
	cetakArray(arr, N);
	return 0;
}
